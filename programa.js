//crear objeto (variable compuestas) para representar el visor
//L representa a la biblioteca leaflet
let miMapa= L.map('mapid');

//determinar la vista inicial, (ubicacion[bogota],zoom)

miMapa.setView([4.604583,-74.108058],16)

let miProveedor=L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png')
miProveedor.addTo(miMapa);
let jac= L.marker([4.603097,-74.108305])
jac.addTo(miMapa)

let circle = L.circle([4.604583,-74.108058], {
    color: 'orange',
    fillColor: '#f3',
    fillOpacity: 0.5,
    radius: 230
})
circle.addTo(miMapa)
let pol= L.polygon([
    [4.604685,-74.107784],
    [4.604893,-74.107645],
    [4.605118,-74.107951],
    [4.604910,-74.108112]
])
pol.addTo(miMapa)